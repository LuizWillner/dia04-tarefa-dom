const msgList = document.querySelector("#chat");
var id = 0;

function divMaker (message) {
    let newElement = document.createElement("div");  // criando um novo elemento HTML de tag <div>
    // newElement.id = "msg" + id;  determinando uma id (nome) para o elemento criado
    newElement.classList.add("message");  // adicionando elemento na lista de id = "chat"
    // newElement.id = `msg-${id}`
    newElement.innerHTML = `
        <div class="message-txt">
            <p>${message}</p>
        </div>

        <div class="message-options">
            <button class="edit-button">Editar</button>
            <button class="delete-button">Deletar</button>
        </div>`;

    newElement.querySelector(".delete-button").addEventListener("click", (e) => {
        console.log(e);
        e.target.parentElement.parentElement.remove();

    })

    msgList.appendChild(newElement);

    console.log(newElement);

    id++;


}


// function deleteMessage() {

// }

document.querySelector("#msgToSend").addEventListener("submit", (e) => {
    e.preventDefault();
    let typedMsg = document.querySelector("#typedTxt").value;
    console.log("typedMsg = " + typedMsg);
    divMaker(typedMsg);

    deleteButton.addEventListener('click', ()=>{
        event.target.parentNode.parentNode.remove();
    });
})



/* <div class="message-txt">
    <p>${message}</p>
</div>

<div class="message-options">
    <button onclick="switchButtons(${id})">Editar</button>
    <button onclick="deleteMessage(${id})">Deletar</button>
</div>

<div class="message-edit">
    <input type="text" id="txt-edit-${id}" value="${message}">
    <button onclick="saveEdit(${id})">Salvar</button>
    <button onclick="switchButtons(${id})">Cancelar</button>
</div>


botaoExcluir.addEventListener('click', ()=>{
    event.target.parentNode.remove();
});
*/
